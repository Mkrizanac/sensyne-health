## Installation
Clone this repository and import into **Android Studio**
```
git clone git@bitbucket.org:Mkrizanac/sensyne-health.git
```

## Project architecture

#### Presentation layer
    Layer containing Activities/Fragments and their corresponding ViewModels.
    This layer also contains: 
    - Actions 
    - Events 
    - ViewStates
    
#### Domain layer
    Layer containing:
    - Repository interfaces
    - UseCases 
    - Domain models 
    - Mapper interfaces
    
#### Data layer
    Layer containing:
    - Repository implementations 
    - Network response models 
    - Network APIs 
    - Mapper implementations

### Data flow between View and ViewModel
    Communication between Views and ViewModels is done using:
    - Action class (User actions)
    - Event class (UI Events that don't need to be shown again after, for example, configuration change)
    - ViewState class (Current state of the view)
    
    For handling ViewState StateFlow is used. 
    For handling Events Channels are used.
    For handling Actions SharedFlow is used

## Dependecies

- Kotlin
- Kotlin Coroutines
- Hilt
- Retrofit
- Espresso
- Mockito