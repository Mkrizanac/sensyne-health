package hr.mate.sensynehealth.presentation.details

import hr.mate.sensynehealth.base.BaseAction
import hr.mate.sensynehealth.domain.models.Hospital

sealed class DetailsAction : BaseAction {
    data class BindHospital(val hospital: Hospital) : DetailsAction()
    object PhoneNumberClicked : DetailsAction()
}
