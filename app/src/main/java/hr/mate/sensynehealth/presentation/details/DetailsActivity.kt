package hr.mate.sensynehealth.presentation.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.util.Linkify
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import hr.mate.sensynehealth.databinding.ActivityDetailsBinding
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.utils.createDialIntent
import hr.mate.sensynehealth.utils.setTextOrHide
import hr.mate.sensynehealth.utils.setupToolbar
import hr.mate.sensynehealth.utils.subscribe
import kotlinx.coroutines.flow.collect

class DetailsActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_HOSPITAL = "extra_details_hospital"

        fun createIntent(context: Context, hospital: Hospital) =
            Intent(context, DetailsActivity::class.java).apply {
                putExtra(EXTRA_HOSPITAL, hospital)
                context.startActivity(this)
            }
    }

    private lateinit var binding: ActivityDetailsBinding
    private val viewModel: DetailsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar(binding.toolbar.root)

        subscribeToViewState()
        subscribeToEvents()
        extractHospital()
        setClickListeners()
    }

    private fun extractHospital() {
        intent.getParcelableExtra<Hospital>(EXTRA_HOSPITAL)?.let {
            viewModel.setAction(DetailsAction.BindHospital(it))
        }
    }

    private fun eventReceived(event: DetailsEvent) = when (event) {
        is DetailsEvent.DialPhone -> dialPhoneNumber(event.phone)
    }

    private fun viewStateChanged(state: DetailsViewState) = when (state) {
        is DetailsViewState.HospitalDataViewState -> renderHospitalDate(
            state.toolbarTitle,
            state.code,
            state.name,
            state.sector,
            state.parent,
            state.email,
            state.phone,
            state.location,
            state.website
        )
        is DetailsViewState.Loading -> showProgress()
    }

    private fun renderHospitalDate(
        toolbarTitle: String,
        code: String,
        name: String,
        sector: String,
        parent: String,
        email: String,
        phone: String,
        location: String,
        website: String
    ) {
        hideProgress()
        binding.toolbar.title.setTextOrHide(toolbarTitle)
        binding.code.setTextOrHide(code)
        binding.name.setTextOrHide(name)
        binding.sector.setTextOrHide(sector)
        binding.parent.setTextOrHide(parent)
        binding.email.setTextOrHide(email)
        binding.phone.setTextOrHide(phone)
        binding.location.setTextOrHide(location)
        binding.website.setTextOrHide(website)

        Linkify.addLinks(binding.website, Linkify.WEB_URLS)
        Linkify.addLinks(binding.email, Linkify.EMAIL_ADDRESSES)
    }

    private fun dialPhoneNumber(phone: String) {
        createDialIntent(phone)
    }

    private fun showProgress() {
        binding.progress.isVisible = true
    }

    private fun hideProgress() {
        binding.progress.isVisible = false
    }

    private fun setClickListeners() {
        binding.phone.setOnClickListener {
            viewModel.setAction(DetailsAction.PhoneNumberClicked)
        }
    }

    private fun subscribeToViewState() {
        subscribe {
            viewModel.viewState.collect {
                viewStateChanged(it)
            }
        }
    }

    private fun subscribeToEvents() {
        subscribe {
            viewModel.event.collect {
                eventReceived(it)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        if (item.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
}
