package hr.mate.sensynehealth.presentation.details

import hr.mate.sensynehealth.base.BaseViewState

sealed class DetailsViewState : BaseViewState {
    object Loading : DetailsViewState()
    data class HospitalDataViewState(
        val toolbarTitle: String,
        val code: String,
        val name: String,
        val sector: String,
        val location: String,
        val parent: String,
        val email: String,
        val phone: String,
        val website: String
    ) : DetailsViewState()
}
