package hr.mate.sensynehealth.presentation.home

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hr.mate.sensynehealth.base.BaseViewModel
import hr.mate.sensynehealth.base.Result
import hr.mate.sensynehealth.domain.interactors.GetHospitals
import hr.mate.sensynehealth.domain.models.Hospital
import kotlinx.coroutines.launch
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getHospitals: GetHospitals
) : BaseViewModel<HomeViewState, HomeEvent, HomeAction>() {

    companion object {
        const val FILTER_ANIMATION_DURATION = 300L
    }

    private var hospitals: List<Hospital> = listOf()

    init {
        getAllHospitals()
    }

    override fun createInitialState(): HomeViewState = HomeViewState.Loading

    override fun onAction(action: HomeAction) =
        when (action) {
            is HomeAction.PhoneNumberClicked -> setEvent(HomeEvent.DialPhone(action.phone))
            is HomeAction.HospitalItemClicked -> navigateToHospitalDetails(action.organisationId)
            is HomeAction.FilterClicked -> filterClicked()
            is HomeAction.ClearFilter -> deleteFilter()
            is HomeAction.FilterSaved -> filterHospitals(action.filter)
        }

    private fun filterClicked() {
        setEvent(HomeEvent.ToggleFilterView(FILTER_ANIMATION_DURATION))
    }

    private fun deleteFilter() {
        setEvent(HomeEvent.ClearFilter)
        setViewState(HomeViewState.DataViewState(hospitals))
    }

    private fun filterHospitals(filter: String) {
        val filteredList = hospitals.filter {
            it.city.lowercase(Locale.getDefault()).contains(filter.lowercase(Locale.getDefault()))
        }
        if (filteredList.isNotEmpty()) {
            setEvent(HomeEvent.ToggleFilterView(FILTER_ANIMATION_DURATION))
        }
        setViewState(HomeViewState.DataViewState(filteredList))
    }

    private fun navigateToHospitalDetails(organisationId: Int) {
        hospitals.find { it.organisationId == organisationId }?.let {
            setEvent(HomeEvent.NavigateToDetails(it))
        }
    }

    private fun getAllHospitals() {
        viewModelScope.launch {
            when (val result = getHospitals()) {
                is Result.Success -> {
                    result.result?.let {
                        hospitals = it
                        setViewState(HomeViewState.DataViewState(it))
                    } ?: setViewState(HomeViewState.EmptyViewState)
                }
                is Result.Error -> {
                    setEvent(HomeEvent.Error(result.message))
                }
            }
        }
    }
}
