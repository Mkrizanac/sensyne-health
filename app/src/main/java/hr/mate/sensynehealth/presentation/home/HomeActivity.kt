package hr.mate.sensynehealth.presentation.home

import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import dagger.hilt.android.AndroidEntryPoint
import hr.mate.sensynehealth.R
import hr.mate.sensynehealth.databinding.ActivityMainBinding
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.presentation.details.DetailsActivity
import hr.mate.sensynehealth.utils.createDialIntent
import hr.mate.sensynehealth.utils.hideKeyboard
import hr.mate.sensynehealth.utils.setupToolbar
import hr.mate.sensynehealth.utils.showKeyboard
import hr.mate.sensynehealth.utils.subscribe
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var homeAdapter: HomeAdapter

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setToolbar()
        setAdapter()
        subscribeToEvents()
        subscribeToViewState()
        setFilterClickListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.navigationFilter) {
            viewModel.setAction(HomeAction.FilterClicked)
            true
        } else
            super.onOptionsItemSelected(item)
    }

    private fun viewStateChanged(state: HomeViewState) = when (state) {
        HomeViewState.Loading -> showProgress()
        HomeViewState.EmptyViewState -> renderNoData()
        is HomeViewState.DataViewState -> renderData(state.hospitals)
    }

    private fun eventReceived(event: HomeEvent) = when (event) {
        is HomeEvent.Error -> renderErrorMessage(event.errorMessage)
        is HomeEvent.DialPhone -> dialPhoneNumber(event.phone)
        is HomeEvent.NavigateToDetails -> navigateToDetails(event.hospital)
        is HomeEvent.ToggleFilterView -> renderFilter(event.animationDuration)
        is HomeEvent.ClearFilter -> clearFilterInput()
    }

    private fun setAdapter() {
        homeAdapter = HomeAdapter(
            this,
            object : HomeAdapter.Listener {
                override fun itemClicked(id: Int) {
                    viewModel.setAction(HomeAction.HospitalItemClicked(id))
                }

                override fun phoneNumberClicked(phone: String) {
                    viewModel.setAction(HomeAction.PhoneNumberClicked(phone))
                }
            }
        )

        binding.hospitals.layoutManager = LinearLayoutManager(this)
        binding.hospitals.adapter = homeAdapter
        binding.hospitals.setHasFixedSize(true)
    }

    private fun clearFilterInput() {
        binding.filterLayout.filterInput.setText("")
    }

    private fun renderData(list: List<Hospital>) {
        hideProgress()
        homeAdapter.setItems(list)
    }

    private fun showProgress() {
        binding.progress.isVisible = true
    }

    private fun hideProgress() {
        binding.progress.isVisible = false
    }

    private fun renderNoData() {
        binding.noDataContainer.isVisible = true
    }

    private fun renderErrorMessage(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun dialPhoneNumber(phone: String) {
        createDialIntent(phone)
    }

    private fun navigateToDetails(hospital: Hospital) {
        DetailsActivity.createIntent(this, hospital)
    }

    private fun setToolbar() {
        setupToolbar(binding.toolbar.root, false)
        binding.toolbar.title.text = getString(R.string.app_name)
    }

    private fun setFilterClickListeners() {
        binding.filterLayout.deleteFilter.setOnClickListener {
            viewModel.setAction(HomeAction.ClearFilter)
        }

        binding.filterLayout.saveFilter.setOnClickListener {
            val filter = binding.filterLayout.filterInput.text.toString()
            viewModel.setAction(HomeAction.FilterSaved(filter))
        }
    }

    private fun renderFilter(animationDuration: Long) {
        val transition: Transition = Slide(Gravity.TOP)
        transition.duration = animationDuration
        transition.addTarget(binding.filterLayout.filter)

        TransitionManager.beginDelayedTransition(binding.root, transition)
        binding.filterLayout.filter.isVisible = !binding.filterLayout.filter.isVisible

        if (binding.filterLayout.filter.isVisible) {
            binding.filterLayout.filterInput.showKeyboard()
        } else {
            binding.filterLayout.filterInput.hideKeyboard()
        }
    }

    private fun subscribeToViewState() {
        subscribe {
            viewModel.viewState.collect {
                viewStateChanged(it)
            }
        }
    }

    private fun subscribeToEvents() {
        subscribe {
            viewModel.event.collect {
                eventReceived(it)
            }
        }
    }
}
