package hr.mate.sensynehealth.presentation.details

import hr.mate.sensynehealth.base.BaseEvent

sealed class DetailsEvent : BaseEvent {
    data class DialPhone(val phone: String) : DetailsEvent()
}
