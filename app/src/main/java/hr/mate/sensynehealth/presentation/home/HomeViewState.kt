package hr.mate.sensynehealth.presentation.home

import hr.mate.sensynehealth.base.BaseViewState
import hr.mate.sensynehealth.domain.models.Hospital

sealed class HomeViewState : BaseViewState {
    object Loading : HomeViewState()
    object EmptyViewState : HomeViewState()
    data class DataViewState(val hospitals: List<Hospital>) : HomeViewState()
}
