package hr.mate.sensynehealth.presentation.home

import hr.mate.sensynehealth.base.BaseAction

sealed class HomeAction : BaseAction {
    data class PhoneNumberClicked(val phone: String) : HomeAction()
    data class HospitalItemClicked(val organisationId: Int) : HomeAction()
    data class FilterSaved(val filter: String) : HomeAction()
    object ClearFilter : HomeAction()
    object FilterClicked : HomeAction()
}
