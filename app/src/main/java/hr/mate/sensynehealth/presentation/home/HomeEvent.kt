package hr.mate.sensynehealth.presentation.home

import hr.mate.sensynehealth.base.BaseEvent
import hr.mate.sensynehealth.domain.models.Hospital

sealed class HomeEvent : BaseEvent {
    data class Error(val errorMessage: String) : HomeEvent()
    data class DialPhone(val phone: String) : HomeEvent()
    data class NavigateToDetails(val hospital: Hospital) : HomeEvent()
    data class ToggleFilterView(val animationDuration: Long) : HomeEvent()
    object ClearFilter : HomeEvent()
}
