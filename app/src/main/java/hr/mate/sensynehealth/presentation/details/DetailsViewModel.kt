package hr.mate.sensynehealth.presentation.details

import hr.mate.sensynehealth.base.BaseViewModel
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.utils.appendIfNotEmpty

class DetailsViewModel : BaseViewModel<DetailsViewState, DetailsEvent, DetailsAction>() {

    private var hospital: Hospital? = null

    override fun createInitialState(): DetailsViewState = DetailsViewState.Loading

    override fun onAction(action: DetailsAction) = when (action) {
        is DetailsAction.BindHospital -> setHospital(action.hospital)
        DetailsAction.PhoneNumberClicked -> dialPhone()
    }

    private fun dialPhone() {
        hospital?.phone?.let {
            setEvent(DetailsEvent.DialPhone(it))
        }
    }

    private fun setHospital(hospital: Hospital) {
        this.hospital = hospital

        setViewState(
            DetailsViewState.HospitalDataViewState(
                toolbarTitle = hospital.organisationType,
                code = hospital.organisationCode,
                name = hospital.organisationName,
                sector = hospital.sector,
                parent = hospital.parentName,
                email = hospital.email,
                phone = hospital.phone,
                location = createLocationText(hospital),
                website = hospital.website
            )
        )
    }

    private fun createLocationText(hospital: Hospital): String {
        val builder = StringBuilder()
        builder.appendIfNotEmpty(hospital.city)
        builder.appendIfNotEmpty(hospital.county, addComma = true)
        builder.appendIfNotEmpty(hospital.addressFirst, addNewLine = true)
        builder.appendIfNotEmpty(hospital.addressSecond, addComma = true)
        builder.appendIfNotEmpty(hospital.addressThird, addComma = true)

        return builder.toString()
    }
}
