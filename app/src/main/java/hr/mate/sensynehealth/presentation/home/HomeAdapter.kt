package hr.mate.sensynehealth.presentation.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import hr.mate.sensynehealth.databinding.LayoutHospitalItemBinding
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.utils.appendIfNotEmpty

class HomeAdapter(context: Context, private val listener: Listener) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    private val items = mutableListOf<Hospital>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutHospitalItemBinding.inflate(layoutInflater, parent, false), listener)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun setItems(hospitals: List<Hospital>) {
        val result = DiffUtil.calculateDiff(DiffUtilCallback(items, hospitals))
        items.clear()
        items.addAll(hospitals)
        result.dispatchUpdatesTo(this)
    }

    class ViewHolder(
        private val binding: LayoutHospitalItemBinding,
        private val listener: Listener
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(hospital: Hospital) {
            binding.name.text = hospital.organisationName
            binding.location.text = createLocationText(hospital)
            binding.phone.text = hospital.phone

            binding.phone.setOnClickListener {
                listener.phoneNumberClicked(hospital.phone)
            }

            binding.root.setOnClickListener {
                listener.itemClicked(hospital.organisationId)
            }
        }

        private fun createLocationText(hospital: Hospital): String {
            val builder = StringBuilder()
            builder.appendIfNotEmpty(hospital.city)
            builder.appendIfNotEmpty(hospital.county, addComma = true)
            builder.appendIfNotEmpty(hospital.addressFirst, addNewLine = true)
            builder.appendIfNotEmpty(hospital.addressSecond, addComma = true)
            builder.appendIfNotEmpty(hospital.addressThird, addComma = true)

            return builder.toString()
        }
    }

    class DiffUtilCallback(
        private val oldItems: List<Hospital>,
        private val newItems: List<Hospital>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition].organisationId == newItems[newItemPosition].organisationId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }
    }

    interface Listener {
        fun itemClicked(id: Int)
        fun phoneNumberClicked(phone: String)
    }
}
