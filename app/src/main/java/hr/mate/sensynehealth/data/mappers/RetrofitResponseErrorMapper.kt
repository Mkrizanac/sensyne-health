package hr.mate.sensynehealth.data.mappers

import hr.mate.sensynehealth.domain.mappers.ResponseErrorMapper
import hr.mate.sensynehealth.domain.models.Error
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class RetrofitResponseErrorMapper(
    private val retrofit: Retrofit
) : ResponseErrorMapper {

    override fun mapError(response: Response<*>): Error? {
        val converter = retrofit.responseBodyConverter<Error>(Error::class.java, arrayOfNulls(0))
        return try {
            converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
            Error()
        }
    }
}
