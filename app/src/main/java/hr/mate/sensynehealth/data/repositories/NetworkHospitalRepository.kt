package hr.mate.sensynehealth.data.repositories

import hr.mate.sensynehealth.base.Result
import hr.mate.sensynehealth.data.remote.HospitalApi
import hr.mate.sensynehealth.data.remote.models.HospitalResponse
import hr.mate.sensynehealth.domain.mappers.DataToDomainMapper
import hr.mate.sensynehealth.domain.mappers.ResponseErrorMapper
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.domain.repositories.HospitalRepository
import hr.mate.sensynehealth.utils.Constants
import retrofit2.Response

class NetworkHospitalRepository(
    private val hospitalApi: HospitalApi,
    private val errorMapper: ResponseErrorMapper,
    private val dataToDomainMapper: DataToDomainMapper<HospitalResponse>
) : HospitalRepository {

    override suspend fun getHospitals(): Result<List<Hospital>> {
        return getResponse(
            { hospitalApi.getHospitals() },
            { dataToDomainMapper.dataToDomain(it ?: listOf()) }
        )
    }

    private suspend fun <T, U> getResponse(
        request: suspend () -> Response<T>,
        map: (response: T?) -> U
    ): Result<U> {
        return try {
            val result = request.invoke()
            if (result.isSuccessful) {
                return Result.Success(map.invoke(result.body()))
            } else {
                val errorResponse = errorMapper.mapError(result)
                Result.Error(
                    errorResponse?.statusCode ?: 0,
                    errorResponse?.statusMessage ?: Constants.UNKNOWN_ERROR
                )
            }
        } catch (e: Throwable) {
            Result.Error(0, Constants.UNKNOWN_ERROR)
        }
    }
}
