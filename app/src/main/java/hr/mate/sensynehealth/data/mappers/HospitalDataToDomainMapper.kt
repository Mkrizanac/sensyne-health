package hr.mate.sensynehealth.data.mappers

import hr.mate.sensynehealth.data.remote.models.HospitalResponse
import hr.mate.sensynehealth.domain.mappers.DataToDomainMapper
import hr.mate.sensynehealth.domain.models.Hospital

class HospitalDataToDomainMapper : DataToDomainMapper<HospitalResponse> {

    override fun dataToDomain(items: List<HospitalResponse>): List<Hospital> =
        items.map(this::dataToDomain)

    override fun dataToDomain(item: HospitalResponse): Hospital {
        return Hospital(
            organisationId = item.organisationId,
            organisationCode = item.organisationCode,
            organisationType = item.organisationType,
            subType = item.subType,
            sector = item.sector,
            organisationStatus = item.organisationStatus,
            isPimsManaged = item.isPimsManaged,
            organisationName = item.organisationName,
            addressFirst = item.addressFirst,
            addressSecond = item.addressSecond,
            addressThird = item.addressThird,
            city = item.city,
            county = item.county,
            postcode = item.postcode,
            latitude = item.latitude,
            longitude = item.longitude,
            parentODSCode = item.parentODSCode,
            parentName = item.parentName,
            phone = item.phone,
            email = item.email,
            website = item.website,
            fax = item.fax
        )
    }
}
