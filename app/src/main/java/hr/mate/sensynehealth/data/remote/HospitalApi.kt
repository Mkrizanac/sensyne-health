package hr.mate.sensynehealth.data.remote

import hr.mate.sensynehealth.data.remote.models.HospitalResponse
import retrofit2.Response
import retrofit2.http.GET

interface HospitalApi {
    @GET("hospitals.json")
    suspend fun getHospitals(): Response<List<HospitalResponse>>
}
