package hr.mate.sensynehealth.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

abstract class BaseViewModel<State : BaseViewState, Event : BaseEvent, Action : BaseAction> :
    ViewModel() {

    private val initialState: State by lazy { createInitialState() }
    abstract fun createInitialState(): State

    val currentState: State
        get() = viewState.value

    private val viewStateMutable: MutableStateFlow<State> = MutableStateFlow(initialState)
    val viewState = viewStateMutable.asStateFlow()

    private val actionMutable: MutableSharedFlow<Action> = MutableSharedFlow()
    private val action = actionMutable.asSharedFlow()

    private val eventChannel: Channel<Event> = Channel()
    val event = eventChannel.receiveAsFlow()

    init {
        subscribeToAction()
    }

    private fun subscribeToAction() {
        viewModelScope.launch {
            action.collect {
                onAction(it)
            }
        }
    }

    abstract fun onAction(action: Action)

    fun setAction(newAction: Action) {
        viewModelScope.launch { actionMutable.emit(newAction) }
    }

    protected fun setViewState(newViewState: State) {
        viewStateMutable.value = newViewState
    }

    protected fun setEvent(newEvent: Event) {
        viewModelScope.launch { eventChannel.send(newEvent) }
    }
}
