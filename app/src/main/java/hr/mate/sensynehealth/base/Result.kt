package hr.mate.sensynehealth.base

sealed class Result <T> {
    data class Success<T>(val result: T?) : Result<T>()
    data class Error<T>(val statusCode: Int, val message: String) : Result<T>()
}
