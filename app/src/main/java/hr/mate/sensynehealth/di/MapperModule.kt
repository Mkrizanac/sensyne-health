package hr.mate.sensynehealth.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.mate.sensynehealth.data.mappers.HospitalDataToDomainMapper
import hr.mate.sensynehealth.data.mappers.RetrofitResponseErrorMapper
import hr.mate.sensynehealth.data.remote.models.HospitalResponse
import hr.mate.sensynehealth.domain.mappers.DataToDomainMapper
import hr.mate.sensynehealth.domain.mappers.ResponseErrorMapper
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MapperModule {

    @Singleton
    @Provides
    fun provideErrorMapper(retrofit: Retrofit): ResponseErrorMapper =
        RetrofitResponseErrorMapper(retrofit)

    @Singleton
    @Provides
    fun provideHospitalMapper(): DataToDomainMapper<HospitalResponse> =
        HospitalDataToDomainMapper()
}
