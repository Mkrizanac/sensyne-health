package hr.mate.sensynehealth.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.mate.sensynehealth.data.remote.HospitalApi
import hr.mate.sensynehealth.data.remote.models.HospitalResponse
import hr.mate.sensynehealth.data.repositories.NetworkHospitalRepository
import hr.mate.sensynehealth.domain.mappers.DataToDomainMapper
import hr.mate.sensynehealth.domain.mappers.ResponseErrorMapper
import hr.mate.sensynehealth.domain.repositories.HospitalRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class HomeModule {

    @Singleton
    @Provides
    fun provideHospitalRepository(
        hospitalApi: HospitalApi,
        errorMapper: ResponseErrorMapper,
        dataToDomainMapper: DataToDomainMapper<HospitalResponse>
    ): HospitalRepository = NetworkHospitalRepository(
        hospitalApi,
        errorMapper,
        dataToDomainMapper
    )
}
