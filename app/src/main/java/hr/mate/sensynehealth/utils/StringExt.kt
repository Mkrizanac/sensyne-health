package hr.mate.sensynehealth.utils

fun StringBuilder.appendIfNotEmpty(
    value: String,
    addComma: Boolean = false,
    addDot: Boolean = false,
    addNewLine: Boolean = false
) {
    if (value.isNotEmpty()) {
        if (addComma) this.append(", ")
        if (addDot) this.append(". ")
        if (addNewLine) this.append("\n")
        this.append(value)
    }
}
