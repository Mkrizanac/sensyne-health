package hr.mate.sensynehealth.utils

import android.widget.TextView
import androidx.core.view.isVisible

fun TextView.setTextOrHide(text: String) {
    if (text.isNotEmpty()) {
        this.text = text
    } else {
        this.isVisible = false
    }
}
