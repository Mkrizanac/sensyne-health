package hr.mate.sensynehealth.utils

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import hr.mate.sensynehealth.R
import kotlinx.coroutines.launch

fun AppCompatActivity.subscribe(onReceived: suspend () -> Unit) {
    lifecycleScope.launch {
        repeatOnLifecycle(Lifecycle.State.STARTED) {
            onReceived()
        }
    }
}

fun AppCompatActivity.setupToolbar(toolbar: Toolbar, displayHome: Boolean = true) {
    toolbar.let {
        setSupportActionBar(it)
        supportActionBar?.setDisplayHomeAsUpEnabled(displayHome)
        supportActionBar?.setDisplayShowHomeEnabled(displayHome)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
    }
}

fun AppCompatActivity.createDialIntent(phone: String) {
    val intent = Intent(Intent.ACTION_DIAL).apply {
        data = Uri.parse("tel:$phone")
    }
    startActivity(intent)
}
