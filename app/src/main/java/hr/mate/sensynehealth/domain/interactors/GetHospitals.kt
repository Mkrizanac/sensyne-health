package hr.mate.sensynehealth.domain.interactors

import hr.mate.sensynehealth.base.Result
import hr.mate.sensynehealth.domain.models.Hospital
import hr.mate.sensynehealth.domain.repositories.HospitalRepository
import javax.inject.Inject

class GetHospitals @Inject constructor(private val hospitalRepository: HospitalRepository) {
    suspend operator fun invoke(): Result<List<Hospital>> = hospitalRepository.getHospitals()
}
