package hr.mate.sensynehealth.domain.mappers

import hr.mate.sensynehealth.domain.models.Error
import retrofit2.Response

interface ResponseErrorMapper {
    fun mapError(response: Response<*>): Error?
}
