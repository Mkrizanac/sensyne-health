package hr.mate.sensynehealth.domain.models

data class Error(
    val statusCode: Int = 0,
    val statusMessage: String? = null
)
