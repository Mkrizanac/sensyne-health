package hr.mate.sensynehealth.domain.repositories

import hr.mate.sensynehealth.base.Result
import hr.mate.sensynehealth.domain.models.Hospital

interface HospitalRepository {
    suspend fun getHospitals(): Result<List<Hospital>>
}
