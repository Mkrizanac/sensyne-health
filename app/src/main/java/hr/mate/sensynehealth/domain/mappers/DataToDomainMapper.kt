package hr.mate.sensynehealth.domain.mappers

import hr.mate.sensynehealth.domain.models.Hospital

interface DataToDomainMapper<T> {
    fun dataToDomain(items: List<T>): List<Hospital>
    fun dataToDomain(item: T): Hospital
}
