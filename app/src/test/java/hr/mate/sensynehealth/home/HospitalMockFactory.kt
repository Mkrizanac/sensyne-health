package hr.mate.sensynehealth.home

import hr.mate.sensynehealth.domain.models.Hospital

fun createMockHospitals(): List<Hospital> {
    val hospitals = mutableListOf<Hospital>()
    for (i in 0..10) {
        hospitals.add(createMockHospital(returnCityName(i), i))
    }

    return hospitals
}

fun createMockHospital(city: String, id: Int): Hospital =
    Hospital(
        organisationId = id,
        organisationCode = "organisationCode",
        organisationType = "organisationType",
        subType = "subType",
        sector = "sector",
        organisationStatus = "organisationStatus",
        isPimsManaged = "isPimsManaged",
        organisationName = "organisationName",
        addressFirst = "addressFirst",
        addressSecond = "addressSecond",
        addressThird = "addressThird",
        city = city,
        county = "county",
        postcode = "postcode",
        latitude = "latitude",
        longitude = "longitude",
        parentODSCode = "parentODSCode",
        parentName = "parentName",
        phone = "phone",
        email = "email",
        website = "website",
        fax = "fax"
    )

fun returnCityName(index: Int): String = when (index) {
    0 -> "Omis"
    2 -> "Zagreb"
    3 -> "Split"
    4 -> "Zadar"
    else -> "City"
}
