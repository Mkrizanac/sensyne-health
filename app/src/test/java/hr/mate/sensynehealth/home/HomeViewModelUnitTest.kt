package hr.mate.sensynehealth.home

import hr.mate.sensynehealth.CoroutinesTestRule
import hr.mate.sensynehealth.base.Result
import hr.mate.sensynehealth.domain.interactors.GetHospitals
import hr.mate.sensynehealth.presentation.home.HomeAction
import hr.mate.sensynehealth.presentation.home.HomeEvent
import hr.mate.sensynehealth.presentation.home.HomeViewModel
import hr.mate.sensynehealth.presentation.home.HomeViewState
import hr.mate.sensynehealth.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.Locale

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelUnitTest {

    companion object {
        const val MOCK_CITY_NAME = "Omis"
        const val MOCK_ITEM_CLICKED_ID = 0
        const val MOCK_PHONE_NUMBER = "+38599445512"
    }

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var getHospitals: GetHospitals

    private lateinit var homeViewModel: HomeViewModel

    private val mockHospitals = createMockHospitals()

    @Before
    fun setUp() {
        runTest {
            whenever(getHospitals()).thenReturn(
                Result.Success(mockHospitals)
            )
        }
        homeViewModel = HomeViewModel(getHospitals)
    }

    @Test
    fun `when viewModel is initialized then render DataViewState`() {
        renderDataState()
    }

    @Test
    fun `when filter is saved then render DataViewState with filtered list`() {
        filterByCity(MOCK_CITY_NAME)
        renderFilteredList(MOCK_CITY_NAME)
    }

    @Test
    fun `when hospital item is clicked then navigate to details`() {
        hospitalItemClicked(mockHospitals[MOCK_ITEM_CLICKED_ID].organisationId)
        renderHospitalDetails(MOCK_ITEM_CLICKED_ID)
    }

    @Test
    fun `when filter icon is clicked then render filter state`() {
        filterIconClicked()
        renderFilterView()
    }

    @Test
    fun `when phone number is clicked then dial phone number`() {
        phoneNumberClicked()
        dialPhoneNumber()
    }

    @Test
    fun `when delete filter is clicked then render data view state`() {
        filterByCity(MOCK_CITY_NAME)
        deleteFilterClicked()
        renderDataState()
    }

    private fun renderDataState() {
        assertEquals(
            homeViewModel.viewState.value,
            HomeViewState.DataViewState(mockHospitals)
        )
    }

    private fun renderFilteredList(city: String) {
        val filteredList = mockHospitals.filter {
            it.city.lowercase(Locale.getDefault()).contains(city.lowercase(Locale.getDefault()))
        }
        assertEquals(
            HomeViewState.DataViewState(filteredList),
            homeViewModel.viewState.value
        )
    }

    private fun renderHospitalDetails(id: Int) {
        runTest {
            assertEquals(
                HomeEvent.NavigateToDetails(mockHospitals.first { it.organisationId == id }),
                homeViewModel.event.first()
            )
        }
    }

    private fun renderFilterView() {
        runTest {
            assertEquals(
                HomeEvent.ToggleFilterView(HomeViewModel.FILTER_ANIMATION_DURATION),
                homeViewModel.event.first()
            )
        }
    }

    private fun dialPhoneNumber() {
        runTest {
            assertEquals(HomeEvent.DialPhone(MOCK_PHONE_NUMBER), homeViewModel.event.first())
        }
    }

    private fun filterByCity(city: String) {
        homeViewModel.setAction(HomeAction.FilterSaved(city))
    }

    private fun hospitalItemClicked(id: Int) {
        homeViewModel.setAction(HomeAction.HospitalItemClicked(id))
    }

    private fun filterIconClicked() {
        homeViewModel.setAction(HomeAction.FilterClicked)
    }

    private fun phoneNumberClicked() {
        homeViewModel.setAction(HomeAction.PhoneNumberClicked(MOCK_PHONE_NUMBER))
    }

    private fun deleteFilterClicked() {
        homeViewModel.setAction(HomeAction.ClearFilter)
    }
}
