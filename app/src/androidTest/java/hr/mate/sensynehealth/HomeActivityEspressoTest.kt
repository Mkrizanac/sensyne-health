package hr.mate.sensynehealth

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import hr.mate.sensynehealth.presentation.home.HomeActivity
import hr.mate.sensynehealth.presentation.home.HomeAdapter
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeActivityEspressoTest {

    companion object {
        const val FILTER_TO_BE_TYPED = "brentwood"
        const val ITEM_POSITION_TO_BE_CLICKED = 0
    }

    @get:Rule
    var activityRule: ActivityScenarioRule<HomeActivity> =
        ActivityScenarioRule(HomeActivity::class.java)

    @Test
    fun homeTest() {
        filterItems()
        deleteFilter()
        clickRecyclerViewItem()
    }

    private fun filterItems() {
        onView(withId(R.id.navigationFilter)).perform(click())
        onView(withId(R.id.filterInput)).perform(typeText(FILTER_TO_BE_TYPED))
        onView(withId(R.id.filterLayout)).check(matches(isDisplayed()))
        onView(withId(R.id.saveFilter)).perform(click())
        onView(withId(R.id.filterLayout)).check(matches(not(isDisplayed())))
    }

    private fun deleteFilter() {
        onView(withId(R.id.navigationFilter)).perform(click())
        onView(withId(R.id.deleteFilter)).perform(click())
        onView(withId(R.id.filterLayout)).check(matches(isDisplayed()))
        onView(withId(R.id.navigationFilter)).perform(click())
    }

    private fun clickRecyclerViewItem() {
        onView(withId(R.id.hospitals)).perform(
            RecyclerViewActions.actionOnItemAtPosition<HomeAdapter.ViewHolder>(
                ITEM_POSITION_TO_BE_CLICKED, click()
            )
        )
    }
}
